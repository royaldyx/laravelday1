<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authController extends Controller
{
    public function register()
    {
        return view('halaman.register');
    }
    public function send(request $request)
    {
        $namadepan = $request->firstname;
        $namabelakang = $request->lastname;

        return view('halaman.home', compact('namadepan','namabelakang'));

    }
}
