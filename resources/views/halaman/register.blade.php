<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <form action="/send" method="POST">
        @csrf
        <label>First Name :</label><br>
        <input type="text" name="firstname"><br>

        <label>Last Name :</label><br>
        <input type="text" name="lastname"><br><br>
        
        <label>Gender : </label><br>
            <label><input type="radio" name="gender" value="laki-laki" /> Men</label><br>
            <label><input type="radio" name="gender" value="perempuan" /> Women</label><br>
            <label><input type="radio" name="gender" value="perempuan" /> Other</label><br>

        <p>
            <label>Nationality</label>
            <select name="nationality">
                <option value="malaysia">malaysia</option>
                <option value="brunei">brunei</option>
                <option value="singapur">singapur</option>
                <option value="indonesia">indonesia</option>
                <option value="thailand">thailand</option>
            </select>
        </p>
        <p>
            <label>Language Spoken : </label><br>
            <input type="checkbox" name="bahasa" value="Bahasa Indonesia">Bahasa Indonesia</input><br>
            <input type="checkbox" name="bahasa" value="English">English</input><br>
            <input type="checkbox" name="bahasa" value="Arabic">Arabic</input><br>
            <input type="checkbox" name="bahasa" value="Japanese">Japanese</input><br>
        </p>
        <p>
            <label>Bio : </label><br>
        </p>
        <p>
            <textarea name="bio" cols="30" rows="10"></textarea>
        </p>
        <p>
        <input type="submit" value="Sign Up">
        </p>
    </form>
</body>
</html>